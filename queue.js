let collection = [];
// Write the queue functions below.

function enqueue(element) {
    collection[collection.length] = element; 
    return collection;
}

function print() {
    return collection;
}


function dequeue() {
    for (let index = 1; index < collection.length; index++) {
        collection[index - 1] = collection[index];
        collection.length--;
    }
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
   return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
